package com.verde.config;

import com.verde.module.user.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.filter.CorsFilter;

import javax.inject.Inject;

@Configuration
@EnableWebSecurity
@ComponentScan
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CorsFilter corsFilter;

    @Inject
    private UserDetailsServiceImpl userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.addFilterBefore(corsFilter, ChannelProcessingFilter.class)
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/product").access("hasRole('ADMIN', 'CLIENT')")
                .antMatchers("/productorder").access("hasRole('ADMIN', 'CLIENT')")
                .antMatchers("/order").access("hasRole('ADMIN', 'CLIENT')")
                .antMatchers("/apriori").access("hasRole('ADMIN', 'CLIENT')")
                .antMatchers("/delivery").access("hasRole('ADMIN', 'CLIENT')")
                .antMatchers("/category").access("hasRole('ADMIN', 'CLIENT')")
                .antMatchers("/unit").access("hasRole('ADMIN', 'CLIENT')")
                .antMatchers("/userrole").access("hasRole('ADMIN', 'CLIENT')")
                .antMatchers("/user").permitAll()
                .and()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }
}