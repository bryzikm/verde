package com.verde.module.delivery.service;

import com.verde.module.delivery.DeliveryType;

import java.util.List;

public interface DeliveryTypeService {

    DeliveryType findOneById(Integer id);

    List<DeliveryType> getAllDeliveryTypes();
}
