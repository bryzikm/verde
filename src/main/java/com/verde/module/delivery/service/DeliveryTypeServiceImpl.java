package com.verde.module.delivery.service;

import com.verde.module.delivery.DeliveryType;
import com.verde.module.delivery.repository.DeliveryTypeRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("deliveryTypeService")
@Transactional
public class DeliveryTypeServiceImpl implements DeliveryTypeService {

    @Resource
    private DeliveryTypeRepository deliveryTypeRepository;

    @Override
    public DeliveryType findOneById(Integer id) {
        return this.deliveryTypeRepository.findOne(id);
    }

    @Override
    public List<DeliveryType> getAllDeliveryTypes() {
        return this.deliveryTypeRepository.findAll();
    }
}
