package com.verde.module.delivery.controller;

import com.verde.module.delivery.DeliveryType;
import com.verde.module.delivery.service.DeliveryTypeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping(value = "/delivery")
public class DeliveryTypeController {

    @Inject
    private DeliveryTypeService deliveryTypeService;

    @GetMapping(value = "/all")
    public ResponseEntity<List<DeliveryType>> getAllDeliveryTypes() {
        return new ResponseEntity<>(this.deliveryTypeService.getAllDeliveryTypes(), HttpStatus.OK);
    }
}
