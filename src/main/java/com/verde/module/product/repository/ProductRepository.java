package com.verde.module.product.repository;

import com.verde.module.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findOneById(Integer id);

    Product findOneByName(String name);

    List<Product> findAll();

    List<Product> findByCategory(String category);

    @Query("SELECT p FROM Product p WHERE p.id IN " +
            "(SELECT po.productOrderKey.productId FROM ProductOrder po WHERE po.productOrderKey.orderId = :orderId)")
    List<Product> findOrderProducts(@Param("orderId") Integer orderId);

    @Query("SELECT p FROM Product p WHERE p.id IN " +
            "(SELECT po.productOrderKey.productId FROM ProductOrder po " +
            "WHERE po.productOrderKey.productId = :productId AND po.productOrderKey.orderId = :orderId)")
    Product findProductFromOrder(@Param("productId") Integer productId, @Param("orderId") Integer orderId);
}
