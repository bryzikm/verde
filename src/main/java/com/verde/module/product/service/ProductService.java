package com.verde.module.product.service;

import com.verde.module.product.Product;

import java.util.List;
import java.util.Map;

public interface ProductService {

    void save(Product product);

    void delete(Integer id);

    Product findOneByName(String name);

    List<Product> findOrderProducts(Integer orderId);

    List<Product> getAllProducts();

    List<Map<String, Object>> getOrderProducts(Integer orderId);

    Product findProductFromOrder(Integer productId, Integer orderId);

    Boolean checkIfProductIsOrdered(Integer productId, Integer orderId);
}
