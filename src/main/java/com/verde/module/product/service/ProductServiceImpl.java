package com.verde.module.product.service;

import com.verde.module.product.Product;
import com.verde.module.product.repository.ProductRepository;
import com.verde.module.productorder.ProductOrder;
import com.verde.module.productorder.repository.ProductOrderRepository;
import com.verde.module.productorder.service.ProductOrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService {

    @Resource
    private ProductRepository productRepository;

    @Resource
    private ProductOrderService productOrderService;

    @Override
    public void save(Product product) {
        productRepository.save(product);
    }

    @Override
    public void delete(Integer id) {
        this.productRepository.delete(id);
    }

    @Override
    public Product findOneByName(String name) {
        return this.productRepository.findOneByName(name);
    }

    @Override
    public List<Product> findOrderProducts(Integer orderId) {
        return this.productRepository.findOrderProducts(orderId);
    }

    @Override
    public List<Product> getAllProducts() {
        return this.productRepository.findAll();
    }

    @Override
    public List<Map<String, Object>> getOrderProducts(Integer orderId) {

        return this.mapProductWithQuantity(this.productRepository.findOrderProducts(orderId), orderId);
    }

    @Override
    public Product findProductFromOrder(Integer productId, Integer orderId) {
        return this.productRepository.findProductFromOrder(productId, orderId);
    }

    @Override
    public Boolean checkIfProductIsOrdered(Integer productId, Integer orderId) {

        if(this.productRepository.findProductFromOrder(productId, orderId) != null) {
            return true;
        }

        return false;
    }

    private List<Map<String, Object>> mapProductWithQuantity(List<Product> products, Integer orderId) {
        List<Map<String, Object>> mappedProducts = new ArrayList<>();
        Map<String, Object> mappedProduct;
        ProductOrder productOrder;

        for(Product product: products) {
            mappedProduct = new HashMap<>();
            productOrder = productOrderService.findProductOrderByKey(product.getId(), orderId);

            mappedProduct.put("product", product);
            mappedProduct.put("quantity", productOrder.getQuantity());
            mappedProduct.put("value", productOrder.getValue());

            mappedProducts.add(mappedProduct);
        }

        return mappedProducts;
    }
}
