package com.verde.module.product.controller;

import com.verde.module.product.Product;
import com.verde.module.product.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/product")
public class ProductController {

    @Inject
    private ProductService productService;

    @PostMapping(value = "/add")
    public ResponseEntity<List<Product>> addProduct(@RequestBody Product product) {
        productService.save(product);

        return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> products = productService.getAllProducts();

        if(products == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @PostMapping(value = "/edit")
    public ResponseEntity<List<Product>> editProduct(@RequestBody Product product) {
        productService.save(product);

        return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<List<Product>> deleteProduct(@PathVariable("id") Integer productId) {
        productService.delete(productId);

        return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
    }
}
