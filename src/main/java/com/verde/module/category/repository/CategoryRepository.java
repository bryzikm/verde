package com.verde.module.category.repository;

import com.verde.module.category.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    void deleteCategoryById(Integer id);

    Category findOneByName(String name);
}
