package com.verde.module.category.controller;

import com.verde.module.category.Category;
import com.verde.module.category.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {

    @Inject
    private CategoryService categoryService;

    @PostMapping(value = "/add")
    public ResponseEntity<Category> addCategory(@RequestBody Category category) {

        if(categoryService.checkIfCategoryExists(category.getName())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        categoryService.saveCategory(category);

        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Category>> getAllCategories() {
        return new ResponseEntity<>(categoryService.findAllCategories(), HttpStatus.OK);
    }
}
