package com.verde.module.category.service;

import com.verde.module.category.Category;
import com.verde.module.category.repository.CategoryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Resource
    private CategoryRepository categoryRepository;

    @Override
    public void saveCategory(Category category) {
        categoryRepository.save(category);
    }

    @Override
    public void deleteCategory(Integer id) {
        categoryRepository.deleteCategoryById(id);
    }

    @Override
    public Category findCategoryByName(String name) {
        return categoryRepository.findOneByName(name);
    }

    @Override
    public Boolean checkIfCategoryExists(Integer id) {

        return categoryRepository.findOne(id) != null;
    }

    @Override
    public Boolean checkIfCategoryExists(String name) {

        return categoryRepository.findOneByName(name) != null;
    }

    @Override
    public List<Category> findAllCategories() {
        return categoryRepository.findAll();
    }
}
