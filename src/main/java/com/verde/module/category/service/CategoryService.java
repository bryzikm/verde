package com.verde.module.category.service;

import com.verde.module.category.Category;
import java.util.List;

public interface CategoryService {

    void saveCategory(Category category);

    void deleteCategory(Integer id);

    Category findCategoryByName(String name);

    Boolean checkIfCategoryExists(Integer id);

    Boolean checkIfCategoryExists(String name);

    List<Category> findAllCategories();
}
