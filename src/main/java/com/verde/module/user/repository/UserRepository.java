package com.verde.module.user.repository;

import com.verde.module.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findOneById(Integer id);

    User findOneByUsername(String username);

    List<User> findAll();
}
