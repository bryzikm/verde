package com.verde.module.user.service;

import com.verde.module.user.User;
import com.verde.module.user.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;

    @Override
    public void saveUser(User user) {
        this.userRepository.save(user);
    }

    @Override
    public void deleteUser(User user) {
        this.userRepository.delete(user);
    }

    @Override
    public User findUserById(Integer id) {
        return this.userRepository.findOneById(id);
    }

    @Override
    public User findUserByUsername(String username) {
        return this.userRepository.findOneByUsername(username);
    }

    @Override
    public List<User> findAllUsers() {
        return this.userRepository.findAll();
    }

    @Override
    public Boolean checkIfUserWithUsernameExists(String username) {
        return this.userRepository.findOneByUsername(username) != null;
    }
}
