package com.verde.module.user.service;

import com.verde.module.user.User;

import java.util.List;

public interface UserService {

    void saveUser(User user);

    void deleteUser(User user);

    User findUserById(Integer id);

    User findUserByUsername(String username);

    List<User> findAllUsers();

    Boolean checkIfUserWithUsernameExists(String username);
}
