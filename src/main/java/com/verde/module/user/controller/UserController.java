package com.verde.module.user.controller;

import com.verde.module.mailer.Mail;
import com.verde.module.mailer.service.MailService;
import com.verde.module.user.User;
import com.verde.module.user.service.UserService;
import com.verde.module.userrole.service.UserRoleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.xml.ws.Response;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Random;

@CrossOrigin(origins = "*")
@RestController
public class UserController {

    @Inject
    private UserService userService;

    @Inject
    private UserRoleService userRoleService;

    @Inject
    private MailService mailService;

    @RequestMapping("/user")
    private Principal user(Principal user) {
        return user;
    }

    @PostMapping(value = "/user/register")
    private ResponseEntity<User> registerUser(@RequestBody User user) {

        if (this.userService.checkIfUserWithUsernameExists(user.getUsername())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        user.setUserRole(this.userRoleService.findUserRoleByName("CLIENT"));
        user.setDate(new Date());
        this.userService.saveUser(user);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/user/one/{id}")
    public User getUserById(@PathVariable Integer id) {
        return this.userService.findUserById(id);
    }

    @GetMapping(value = "/user/logged")
    public User getLoggedUser(Principal principal) {
        return this.userService.findUserByUsername(principal.getName());
    }

    @GetMapping(value = "/user/all")
    public List<User> getAllUsers(Principal principal) {

        return this.userService.findAllUsers();
    }

    @DeleteMapping(value = "/user/delete/{password}")
    public ResponseEntity<User> deleteUser(@PathVariable("password") String password, Principal principal) {
        User user = userService.findUserByUsername(principal.getName());

        if (password.equals(user.getPassword())) {
            userService.deleteUser(user);

            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @GetMapping(value = "/user/editpassword/{password}+{newPassword}+{repeatedPassword}")
    public ResponseEntity<User> changeUserPassword(@PathVariable("password") String password,
                                                   @PathVariable("newPassword") String newPassword,
                                                   @PathVariable("repeatedPassword") String repeatedPassword,
                                                   Principal principal) {
        User user = userService.findUserByUsername(principal.getName());

        if (password.equals(user.getPassword()) && newPassword.equals(repeatedPassword)) {
            user.setPassword(newPassword);

            userService.saveUser(user);

            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @PostMapping(value = "/user/edit")
    public ResponseEntity<User> editUser(@RequestBody User edittedUser, Principal principal) {
        User user = userService.findUserByUsername(principal.getName());

        user.setName(edittedUser.getName());
        user.setSurname(edittedUser.getSurname());
        user.setCompany(edittedUser.getCompany());

        if(edittedUser.getNIP() != null)
            user.setNIP(edittedUser.getNIP());

        if(edittedUser.getREGON() != null)
            user.setREGON(edittedUser.getREGON());

        user.setPostalCode(user.getPostalCode());
        user.setPlace(edittedUser.getPlace());

        userService.saveUser(user);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(value = "/user/reset/{username:.+}")
    public ResponseEntity<Boolean> resetPassword(@PathVariable("username") String username) {

        if(userService.checkIfUserWithUsernameExists(username)) {
            User user = this.userService.findUserByUsername(username);
            String generatedPassword = this.generatedPassword(username);
            user.setPassword(generatedPassword);

            mailService.sendEmail(username, this.createResetPasswordMail(username, generatedPassword));

            return new ResponseEntity<>(true, HttpStatus.OK);
        }

        return new ResponseEntity<>(false, HttpStatus.FORBIDDEN);
    }

    private String generatedPassword(String login) {
        Random random = new Random();

        return login.split("@")[0] + (random.nextInt(20000 - 10000) + 10000);
    }

    private Mail createResetPasswordMail(String login, String generatedPassword) {
        Mail mail = new Mail();

        mail.setEmailSubject("Verde - resetowanie hasła");
        mail.setEmailBody("Nowe hasło: " + generatedPassword + ". Prosimy o jego zmianę." );

        return mail;
    }
}
