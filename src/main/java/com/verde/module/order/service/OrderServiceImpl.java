package com.verde.module.order.service;

import com.verde.module.order.Order;
import com.verde.module.order.repository.OrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("orderService")
@Transactional
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderRepository orderRepository;

    @Override
    public void save(Order order) {
        orderRepository.save(order);
    }

    @Override
    public void delete(Integer id) {
        this.orderRepository.delete(id);
    }

    @Override
    public List<Order> findAll() {
        return this.orderRepository.findAll();
    }

    @Override
    public Order findUnsolicitedOrder(Integer userId) {
        return orderRepository.findUnsolicitedOrder(userId);
    }

    @Override
    public List<Order> findSolicitedOrders() {
        return this.orderRepository.findSolicitedOrders();
    }

    @Override
    public List<Order> findArchivedOrders() {
        return this.orderRepository.findArchivedOrders();
    }

    @Override
    public Boolean checkIfUnsolicitedOrderExists(Integer userId) {
        if(orderRepository.findUnsolicitedOrder(userId) != null) {
            return true;
        }

        return false;
    }
}
