package com.verde.module.order.service;

import com.verde.module.order.Order;

import java.util.List;

public interface OrderService {

    void save(Order order);

    void delete(Integer id);

    List<Order> findAll();

    Order findUnsolicitedOrder(Integer userId);

    List<Order> findSolicitedOrders();

    List<Order> findArchivedOrders();

    Boolean checkIfUnsolicitedOrderExists(Integer userId);
}
