package com.verde.module.order.controller;

import com.verde.module.delivery.DeliveryType;
import com.verde.module.delivery.service.DeliveryTypeService;
import com.verde.module.mailer.Mail;
import com.verde.module.mailer.service.MailService;
import com.verde.module.order.Order;
import com.verde.module.order.service.OrderService;
import com.verde.module.user.User;
import com.verde.module.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/order")
public class OrderController {

    @Inject
    private OrderService orderService;

    @Inject
    private DeliveryTypeService deliveryTypeService;

    @Inject
    private UserService userService;

    @Inject
    private MailService mailService;

    @GetMapping(value = "/confirm/{deliveryId}")
    public ResponseEntity<Order> confirmOrder(@PathVariable("deliveryId") Integer deliveryId, Principal principal) {
        User user = userService.findUserByUsername(principal.getName());

        if(!orderService.checkIfUnsolicitedOrderExists(user.getId())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        DeliveryType deliveryType = deliveryTypeService.findOneById(deliveryId);
        Order order = orderService.findUnsolicitedOrder(user.getId());

        order.setDeliveryType(deliveryType);
        order.setDate(new Date());
        order.setOrdered(true);

        orderService.save(order);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/solicited")
    public ResponseEntity<List<Order>> getSolicitedOrders() {
        List<Order> orders = this.orderService.findSolicitedOrders();

        if(orders == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping(value = "/archived")
    public ResponseEntity<List<Order>> getArchivedOrders() {
        List<Order> orders = this.orderService.findArchivedOrders();

        if(orders == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @PostMapping(value = "/complete")
    public ResponseEntity<Map<String, List<Order>>> completeOrder(@RequestBody Order order, Principal principal) {
        order.setAccepted(true);

        orderService.save(order);

        List<Order> orders = this.orderService.findSolicitedOrders();
        List<Order> archivedOrders = this.orderService.findArchivedOrders();

        if(orders == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        Map<String, List<Order>> mappedOrders = new HashMap<>();

        mappedOrders.put("orders", orders);
        mappedOrders.put("archivedOrders", archivedOrders);

        mailService.sendEmail(principal.getName(), this.createConfirmMail(order.getId()));

        return new ResponseEntity<>(mappedOrders, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<List<Order>> deleteOrder(@PathVariable("id") Integer id) {
        this.orderService.delete(id);

        return new ResponseEntity<>(this.orderService.findArchivedOrders(), HttpStatus.OK);
    }

    private Mail createConfirmMail(Integer orderId) {
        Mail mail = new Mail();

        mail.setEmailSubject("Zamówienie nr " + orderId);
        mail.setEmailBody("Zamówienie nr " + orderId + " zostało zrealizowane. Produkty zostały przygotowane." );

        return mail;
    }
}
