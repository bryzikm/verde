package com.verde.module.order.repository;

import com.verde.module.order.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

    @Query("SELECT o FROM Order o WHERE o.user.id = :userId AND o.isOrdered = false")
    Order findUnsolicitedOrder(@Param("userId") Integer userId);

    @Query("SELECT o FROM Order o WHERE o.isAccepted = false AND o.isOrdered = true")
    List<Order> findSolicitedOrders();

    @Query("SELECT o FROM Order o WHERE o.isAccepted = true AND o.isOrdered = true")
    List<Order> findArchivedOrders();
}
