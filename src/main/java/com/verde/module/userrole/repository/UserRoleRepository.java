package com.verde.module.userrole.repository;

import com.verde.module.userrole.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

    UserRole findOneByName(String name);

    UserRole findOneById(Integer id);
}
