package com.verde.module.userrole.service;

import com.verde.module.userrole.UserRole;
import com.verde.module.userrole.repository.UserRoleRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service("userRoleService")
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

    @Resource
    private UserRoleRepository userRoleRepository;

    @Override
    public UserRole findUserRoleByName(String name) {
        return this.userRoleRepository.findOneByName(name);
    }

    @Override
    public UserRole findUserRoleById(Integer id) {
        return this.userRoleRepository.findOneById(id);
    }
}
