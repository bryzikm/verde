package com.verde.module.userrole.service;

import com.verde.module.userrole.UserRole;

public interface UserRoleService {

    UserRole findUserRoleByName(String name);

    UserRole findUserRoleById(Integer id);
}
