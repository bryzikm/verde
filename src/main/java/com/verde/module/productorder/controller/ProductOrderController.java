package com.verde.module.productorder.controller;

import com.verde.module.order.Order;
import com.verde.module.order.service.OrderService;
import com.verde.module.product.Product;
import com.verde.module.product.service.ProductService;
import com.verde.module.productorder.ProductOrder;
import com.verde.module.productorder.ProductOrderKey;
import com.verde.module.productorder.service.ProductOrderService;
import com.verde.module.user.User;
import com.verde.module.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/productorder")
public class ProductOrderController {

    @Inject
    private ProductOrderService productOrderService;

    @Inject
    private OrderService orderService;

    @Inject
    private UserService userService;

    @Inject
    private ProductService productService;

    @PostMapping(value = "/add/{value}")
    public ResponseEntity<List<Map<String, Object>>> addProductToOrder(@RequestBody Product product,
                                                                       @PathVariable("value") Integer value, Principal principal) {
        User user = userService.findUserByUsername(principal.getName());

        if (orderService.checkIfUnsolicitedOrderExists(user.getId())) {
            if (this.checkIfProductIsInOrder(product.getId(), user.getId())) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        } else {
            this.createUserOrder(user);
        }

        return new ResponseEntity<>(this.addProductToUnsolicitedOrder(user.getId(), value, product), HttpStatus.OK);
    }

    @GetMapping(value = "/ordered")
    public ResponseEntity<List<Map<String, Object>>> getOrderProducts(Principal principal) {
        User user = userService.findUserByUsername(principal.getName());
        Order order = orderService.findUnsolicitedOrder(user.getId());

        if (order == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        List<Map<String, Object>> mappedProducts = this.productService.getOrderProducts(order.getId());

        if (mappedProducts.size() == 0) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(mappedProducts, HttpStatus.OK);
    }

    @GetMapping(value = "/order/{id}")
    public ResponseEntity<List<Map<String, Object>>> getOrderProducts(@PathVariable("id") Integer orderId) {
        return new ResponseEntity<>(this.productService.getOrderProducts(orderId), HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{productId}")
    public List<Map<String, Object>> deleteProductFromOrder(@PathVariable("productId") Integer productId,
                                                            Principal principal) {

        User user = userService.findUserByUsername(principal.getName());
        Order order = orderService.findUnsolicitedOrder(user.getId());

        this.productOrderService.delete(this.createProductOrderKey(order.getId(), productId));

        return this.productService.getOrderProducts(order.getId());
    }

    private List<Map<String, Object>> addProductToUnsolicitedOrder(Integer userId, Integer quantity, Product product) {
        Order order = orderService.findUnsolicitedOrder(userId);

        order.setSum(order.getSum() + (double) quantity * product.getPrice());
        orderService.save(order);

        ProductOrder productOrder = new ProductOrder();

        productOrder.setProductOrderKey(this.createProductOrderKey(order.getId(), product.getId()));
        productOrder.setQuantity(quantity);
        productOrder.setValue((double) quantity * product.getPrice());

        productOrderService.save(productOrder);

        return this.productService.getOrderProducts(order.getId());
    }

    private ProductOrderKey createProductOrderKey(Integer orderId, Integer productId) {
        ProductOrderKey productOrderKey = new ProductOrderKey();
        productOrderKey.setOrderId(orderId);
        productOrderKey.setProductId(productId);

        return productOrderKey;
    }

    private void createUserOrder(User user) {
        Order order = new Order();
        order.setDate(new Date());
        order.setAccepted(false);
        order.setOrdered(false);
        order.setUser(user);
        order.setDeliveryType(null);
        order.setSum(0.0d);

        orderService.save(order);
    }

    private Boolean checkIfProductIsInOrder(Integer productId, Integer userId) {
        Order order = orderService.findUnsolicitedOrder(userId);

        return productService.checkIfProductIsOrdered(productId, order.getId());
    }
}
