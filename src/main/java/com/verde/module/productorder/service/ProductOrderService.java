package com.verde.module.productorder.service;

import com.verde.module.productorder.ProductOrder;
import com.verde.module.productorder.ProductOrderKey;

public interface ProductOrderService {

    void save(ProductOrder productOrder);

    void delete(ProductOrderKey productOrderKey);

    ProductOrder findProductOrderByKey(Integer productId, Integer orderId);
}
