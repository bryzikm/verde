package com.verde.module.productorder.service;

import com.verde.module.productorder.ProductOrder;
import com.verde.module.productorder.ProductOrderKey;
import com.verde.module.productorder.repository.ProductOrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service("productOrderService")
@Transactional
public class ProductOrderServiceImpl implements ProductOrderService {

    @Resource
    private ProductOrderRepository productOrderRepository;

    @Override
    public void save(ProductOrder productOrder) {
        productOrderRepository.save(productOrder);
    }

    @Override
    public void delete(ProductOrderKey productOrderKey) {
        this.productOrderRepository.delete(productOrderKey);
    }

    @Override
    public ProductOrder findProductOrderByKey(Integer productId, Integer orderId) {
        ProductOrderKey productOrderKey = new ProductOrderKey();
        productOrderKey.setProductId(productId);
        productOrderKey.setOrderId(orderId);

        return this.productOrderRepository.findOne(productOrderKey);
    }
}
