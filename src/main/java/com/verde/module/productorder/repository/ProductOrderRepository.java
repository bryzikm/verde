package com.verde.module.productorder.repository;

import com.verde.module.productorder.ProductOrder;
import com.verde.module.productorder.ProductOrderKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductOrderRepository extends JpaRepository<ProductOrder, ProductOrderKey> {

}
