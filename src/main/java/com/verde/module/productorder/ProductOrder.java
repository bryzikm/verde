package com.verde.module.productorder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "product_order")
public class ProductOrder {

    @EmbeddedId
    private ProductOrderKey productOrderKey;

    @NotNull
    @Column(name = "quantity")
    private Integer quantity;

    @NotNull
    @Column(name = "value")
    private Double value;

    public ProductOrderKey getProductOrderKey() {
        return productOrderKey;
    }

    public void setProductOrderKey(ProductOrderKey productOrderKey) {
        this.productOrderKey = productOrderKey;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
