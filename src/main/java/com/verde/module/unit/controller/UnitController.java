package com.verde.module.unit.controller;

import com.verde.module.unit.Unit;
import com.verde.module.unit.service.UnitService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping(value = "/unit")
public class UnitController {

    @Inject
    private UnitService unitService;

    @PostMapping(value = "/add")
    public ResponseEntity<Unit> addCategory(@RequestBody Unit unit) {

        if(unitService.checkIfUnitExists(unit.getName())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        unitService.saveUnit(unit);

        return new ResponseEntity<>(unit, HttpStatus.OK);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Unit>> getAllCategories() {
        return new ResponseEntity<>(unitService.findAllUnits(), HttpStatus.OK);
    }
}
