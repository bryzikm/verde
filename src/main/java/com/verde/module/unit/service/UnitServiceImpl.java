package com.verde.module.unit.service;

import com.verde.module.unit.Unit;
import com.verde.module.unit.repository.UnitRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("unitService")
@Transactional
public class UnitServiceImpl implements UnitService {

    @Resource
    private UnitRepository unitRepository;

    @Override
    public void saveUnit(Unit unit) {
        unitRepository.save(unit);
    }

    @Override
    public void deleteUnit(Integer id) {
        unitRepository.deleteUnitById(id);
    }

    @Override
    public Unit findUnitByName(String name) {
        return unitRepository.findOneByName(name);
    }

    @Override
    public Boolean checkIfUnitExists(Integer id) {

        return unitRepository.findOne(id) != null;
    }

    @Override
    public Boolean checkIfUnitExists(String name) {

        return unitRepository.findOneByName(name) != null;
    }

    @Override
    public List<Unit> findAllUnits() {
        return this.unitRepository.findAll();
    }
}
