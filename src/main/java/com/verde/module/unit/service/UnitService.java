package com.verde.module.unit.service;

import com.verde.module.unit.Unit;

import java.util.List;

public interface UnitService {

    void saveUnit(Unit unit);

    void deleteUnit(Integer id);

    Unit findUnitByName(String name);

    Boolean checkIfUnitExists(Integer id);

    Boolean checkIfUnitExists(String name);

    List<Unit> findAllUnits();
}
