package com.verde.module.unit.repository;

import com.verde.module.unit.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UnitRepository extends JpaRepository<Unit, Integer> {

    void deleteUnitById(Integer id);

    Unit findOneByName(String name);
}
