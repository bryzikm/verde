package com.verde.module.mailer;

public class Mail {
    private static final String EMAIL_ADDRESS = "hurtownia.verde@gmail.com";
    private static final String EMAIL_PASSWORD = "verde45370";
    private String emailSubject;
    private String emailBody;

    public static String getEmailAddress() {
        return EMAIL_ADDRESS;
    }

    public static String getEmailPassword() {
        return EMAIL_PASSWORD;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }
}
