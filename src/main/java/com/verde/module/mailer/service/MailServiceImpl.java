package com.verde.module.mailer.service;

import com.verde.module.mailer.Mail;
import com.verde.module.mailer.property.Property;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Service("mailService")
@Transactional
public class MailServiceImpl implements MailService {

    @Override
    public Boolean sendEmail(String receiver, Mail mail) {

        try {
            Properties props = Property.set();
            Authenticator auth = new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Mail.getEmailAddress(), Mail.getEmailPassword());
                }
            };

            Session session = Session.getInstance(props, auth);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(Mail.getEmailAddress());
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver));
            message.setSubject(mail.getEmailSubject());
            message.setText(mail.getEmailBody());
            Transport.send(message);

            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
