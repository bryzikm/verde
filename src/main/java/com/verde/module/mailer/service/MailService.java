package com.verde.module.mailer.service;

import com.verde.module.mailer.Mail;

import java.security.Principal;

public interface MailService {

    Boolean sendEmail(String receiver, Mail mail);
}
