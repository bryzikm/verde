package com.verde.module.apriori.service;

import com.verde.module.apriori.Apriori;
import com.verde.module.apriori.AssociationRule;
import com.verde.module.apriori.Element;
import com.verde.module.apriori.Elementset;
import com.verde.module.order.Order;
import com.verde.module.order.service.OrderService;
import com.verde.module.product.Product;
import com.verde.module.product.service.ProductService;
import com.verde.module.productorder.service.ProductOrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Service("aprioriService")
@Transactional
public class AprioriService {

    @Inject
    private Apriori apriori;

    @Inject
    private ProductService productService;

    @Inject
    private ProductOrderService productOrderService;

    @Inject
    private OrderService orderService;


    public List<Product> getAssociationRules() {

        List<List<Product>> aprioriPurchaseproducts = new ArrayList<>();

        for (Order order : orderService.findAll()) {
            List<Product> purchaseProducts = productService.findOrderProducts(order.getId());
            aprioriPurchaseproducts.add(purchaseProducts);
        }

        Collection associationRules = apriori.run(aprioriPurchaseproducts);

        Iterator associationRuleIterator = associationRules.iterator();

        HashMap<Product, List<Product>> resultMap = new HashMap<>();
        while (associationRuleIterator.hasNext()) {

            AssociationRule associationRule = (AssociationRule) associationRuleIterator
                    .next();

            Elementset leftSide = associationRule.getLeftSide();
            if (leftSide.size() == 1) {
                Product leftSideProduct = productService.findOneByName(leftSide.getFirst().getValue());

                List<Product> leftSideFromMap = resultMap.get(leftSideProduct);

                Elementset rightSide = associationRule.getRightSide();
                List<Product> rightSideProductList = new ArrayList<>();

                Iterator rightSideIterator = rightSide.getIterator();
                while (rightSideIterator.hasNext()) {

                    Element rightSideTempElement = (Element) rightSideIterator.next();
                    rightSideProductList.add(productService.findOneByName(rightSideTempElement.getValue()));
                }

                if (leftSideFromMap != null && (leftSideFromMap.size() < rightSide.size())) {
                    resultMap.put(leftSideProduct, rightSideProductList);
                } else if (leftSideFromMap == null) {
                    resultMap.put(leftSideProduct, rightSideProductList);
                }
            }

        }

        return this.convertFromMapToList(resultMap);
    }

    private List<Product> convertFromMapToList(HashMap<Product, List<Product>> resultMap) {
        List<Product> result = new ArrayList<>();

        for (Map.Entry<Product, List<Product>> mapEntry : resultMap.entrySet()) {
            this.mapProducts(result, mapEntry.getValue());
            result.add(0, mapEntry.getKey());

            break;
        }

        return result;
    }

    private void mapProducts(List<Product> result, List<Product> mapEntry) {

        result.addAll(mapEntry.stream().collect(Collectors.toList()));
    }
}
