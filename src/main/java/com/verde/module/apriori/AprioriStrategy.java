package com.verde.module.apriori;

import java.util.*;

public class AprioriStrategy extends AprioriAbstract {

    private static final int MAX_ELEMENTS_AMOUNT = 500;

    public AprioriStrategy(Dataset dataset) {
        super(dataset);
    }

    @Override
    public Double computeSupport(Elementset elementset) {

        int occurrenceCount = 0;

        Iterator transactionsIterator = this.dataset.getTransactionIterator();

        while (transactionsIterator.hasNext()) {
            Elementset elementSet = (Elementset) transactionsIterator.next();

            if (elementSet.intersect(elementset).size() == elementset.size()) {
                occurrenceCount++;
            }
        }

        return ((double) occurrenceCount) / this.dataset.getNumTransactions();
    }

    @Override
    public Double computeConfidence(AssociationRule associationRule) {

        Elementset union = associationRule.getLeftSide().union(
                associationRule.getRightSide());

        return computeSupport(union)
                / computeSupport(associationRule.getLeftSide());
    }

    public Set getAllElementsetsOfSizeOne() {
        Iterator itItemset = this.dataset.getTransactionIterator();
        Elementset allItems = new Elementset();

        while (itItemset.hasNext()) {
            Elementset elementset = (Elementset) itItemset.next();

            allItems = allItems.union(elementset);
        }

        HashSet<Elementset> allItemsets = new HashSet<>();
        Iterator itItem = allItems.getIterator();
        while (itItem.hasNext()) {
            Element element = (Element) itItem.next();
            Elementset elementset = new Elementset();
            elementset.add(element);
            allItemsets.add(elementset);
        }

        return allItemsets;
    }

    @Override
    public Collection runApriori(double minimalSupport, double minimalConfidence) {

        Set[] candidates = this.generateCandidatesSet(minimalSupport);

        return this.generateAssociationRules(candidates, minimalConfidence);
    }

    private Set[] generateCandidatesSet(double minimalSupport) {

        Set[] candidates = new Set[AprioriStrategy.MAX_ELEMENTS_AMOUNT];

        candidates[1] = getAllElementsetsOfSizeOne();
        for (int itemNumber = 1; itemNumber < AprioriStrategy.MAX_ELEMENTS_AMOUNT && !candidates[itemNumber].isEmpty(); itemNumber++) {

            candidates[itemNumber + 1] = new HashSet<Elementset>();

            for (Iterator prevoiusElementsetIterator = candidates[itemNumber].iterator(); prevoiusElementsetIterator
                    .hasNext(); ) {

                Elementset previousElementset = (Elementset) prevoiusElementsetIterator.next();

                for (Iterator nextElementsetIterator = candidates[itemNumber].iterator(); nextElementsetIterator
                        .hasNext(); ) {
                    Elementset nextElementSet = (Elementset) nextElementsetIterator.next();

                    if (previousElementset.intersect(nextElementSet).size() == itemNumber - 1) {
                        Elementset candidateElementset = previousElementset.union(nextElementSet);

                        assert (candidateElementset.size() == itemNumber + 1);

                        if (computeSupport(candidateElementset) > minimalSupport) {
                            candidates[itemNumber + 1].add(candidateElementset);
                        }

                    }
                }
            }
        }
        return candidates;
    }

    private Collection generateAssociationRules(Set[] candidates, double minimalConfidence) {
        Collection discoveredAssociationRules = new LinkedList<AssociationRule>();

        for (int itemNumber = 1; itemNumber < AprioriStrategy.MAX_ELEMENTS_AMOUNT && !candidates[itemNumber].isEmpty(); itemNumber++) {

            for (Iterator elementsetCandidateIterator = candidates[itemNumber].iterator(); elementsetCandidateIterator.hasNext(); ) {

                Elementset elementsetCandidate = (Elementset) elementsetCandidateIterator.next();


                for (Iterator candidateElementsetIterator = elementsetCandidate
                        .prepareNotEmptySubsets().iterator(); candidateElementsetIterator
                             .hasNext(); ) {

                    Elementset elementsetSubset = (Elementset) candidateElementsetIterator.next();

                    Elementset elementsetA = elementsetSubset;
                    Elementset elementsetB = elementsetCandidate.removeAll(elementsetSubset);

                    AssociationRule candidateAssociationRule = new AssociationRule(
                            elementsetA, elementsetB);

                    if (computeConfidence(candidateAssociationRule) > minimalConfidence) {
                        discoveredAssociationRules.add(candidateAssociationRule);
                    }
                }
            }
        }

        return discoveredAssociationRules;
    }
}

