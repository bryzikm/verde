package com.verde.module.apriori.controller;

import com.verde.module.apriori.service.AprioriService;
import com.verde.module.product.Product;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/apriori")
public class AprioriController {

    @Inject
    private AprioriService aprioriService;

    @GetMapping(value = "/products")
    public List<Product> getRecommendedProducts() {
        return this.aprioriService.getAssociationRules();
    }
}
