package com.verde.module.apriori;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

public class Elementset {

    private Set elements = new HashSet();

    public Elementset() {}

    private Elementset(Elementset elementset) {
        this.elements = new HashSet(elementset.getElements());
    }

    public void add(Element element) {
        this.elements.add(element);
    }

    public Elementset intersect(Elementset otherSet) {
        Elementset newElementset = new Elementset(this);
        newElementset.getElements().retainAll(otherSet.getElements());
        return newElementset;
    }

    public Elementset union(Elementset otherSet) {
        Elementset newElementset = new Elementset(this);
        newElementset.getElements().addAll(otherSet.getElements());
        return newElementset;
    }

    public Elementset removeAll(Elementset otherSet) {
        Elementset newElementset = new Elementset(this);
        newElementset.getElements().removeAll(otherSet.getElements());
        return newElementset;
    }

    private void prepareNotEmptySubsets(Vector itemsVector, int setLevel,
                                        Set notEmptySubsets, Elementset currentElementset) {

        currentElementset = new Elementset(currentElementset);

        boolean itemAdded = false;
        while (true) {

            if (setLevel == itemsVector.size() - 1) {
                if (currentElementset.size() != 0
                        && currentElementset.size() != itemsVector.size()) {
                    notEmptySubsets.add(currentElementset);
                }
            } else {
                prepareNotEmptySubsets(itemsVector, setLevel + 1,
                        notEmptySubsets, currentElementset);
            }
            if (itemAdded) {
                break;
            } else {
                currentElementset = new Elementset(currentElementset);
                currentElementset.add((Element) itemsVector.elementAt(setLevel));

                itemAdded = true;
            }
        }
    }

    public Set prepareNotEmptySubsets() {
        HashSet notEmptySubsets = new HashSet();
        this.prepareNotEmptySubsets(new Vector(this.elements), 0, notEmptySubsets,
                new Elementset());

        return notEmptySubsets;
    }

    public int size() {
        return this.elements.size();
    }

    public Iterator getIterator() {
        return this.elements.iterator();
    }

    public Element getFirst() {
        Iterator itItem = this.elements.iterator();
        Integer i = 0;
        while (itItem.hasNext() && i < 2) {
            return (Element)itItem.next();
        }

        return null;
    }

    public Set getElements() { return this.elements; }

    @Override
    public String toString() {
        StringBuffer out = new StringBuffer();
        Iterator itItem = this.elements.iterator();
        while (itItem.hasNext()) {
            Element element = (Element) itItem.next();
            out.append(element.toString());
            if (itItem.hasNext()) {
                out.append(" ");
            }
        }
        return out.toString();
    }

    @Override
    public boolean equals(Object o) {
        return ((Elementset) o).getElements().equals(this.elements);
    }

    @Override
    public int hashCode() {
        return this.elements.hashCode();
    }
}

