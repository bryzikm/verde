package com.verde.module.apriori;

public class Element {

    private String value;

    public Element(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public boolean equals(Object o) {
        return ((Element) o).value.equals(this.value);
    }

    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public String toString()
    {
        return this.value;
    }

}
