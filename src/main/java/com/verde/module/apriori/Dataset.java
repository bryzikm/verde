package com.verde.module.apriori;

import com.verde.module.product.Product;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Dataset {

    private LinkedList<Elementset> transactions = new LinkedList<>();


    public Dataset(List<List<Product>> productList) throws IOException {

        for(List<Product> products : productList) {
            Elementset newElementset = new Elementset();

            for(Product product : products) {
                newElementset.add(new Element(product.getName()));
            }

            if (newElementset.size() != 0) {
                this.transactions.add(newElementset);
            }
        }
    }

    public Iterator getTransactionIterator() {
        return this.transactions.iterator();
    }

    public int getNumTransactions() {
        return this.transactions.size();
    }

    public LinkedList getTransactions() {
        return this.transactions;
    }
}
