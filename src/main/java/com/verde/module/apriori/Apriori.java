package com.verde.module.apriori;

import com.verde.module.product.Product;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@Component
public class Apriori {

    private Dataset dataset;
    private AprioriAbstract aprioriStrategy;
    private Double minimalSupport = 0.3;
    private Double minimalConfidence = 0.1;

    public Apriori() {
    }

    public Collection run(List<List<Product>> productList) {
        try {
            this.dataset = new Dataset(productList);

            this.aprioriStrategy = new AprioriStrategy(dataset);

            return aprioriStrategy
                    .runApriori(this.minimalSupport, this.minimalConfidence);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private void printAssociationRules(Collection associationRules) {

        Iterator associationRuleIterator = associationRules.iterator();

        while (associationRuleIterator.hasNext()) {

            AssociationRule associationRule = (AssociationRule) associationRuleIterator
                    .next();

            System.out
                    .println("assoctiation rule: "
                            + associationRule
                            + "\t\tsupport: "
                            + this.aprioriStrategy
                            .computeSupport(associationRule
                                    .getLeftSide().union(
                                            associationRule
                                                    .getRightSide()))
                            + "\t\tconfidence: "
                            + this.aprioriStrategy
                            .computeConfidence(associationRule));
        }
    }
}

